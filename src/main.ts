import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { createAuth0 } from '@auth0/auth0-vue'

import axios from 'axios'
import VueAxios from 'vue-axios'


const app = createApp(App);

store.subscribe( (mutation, state) => {
   localStorage.setItem('store', JSON.stringify(state));
})


app.use(store)
   .use(router)
   .use(VueAxios, axios)
   .use (createAuth0 ({
      domain: process.env.VUE_APP_AUTH0_DOMAIN,
      client_id: process.env.VUE_APP_AUTH0_CLIENT_ID,
      connection: "google-oauth2",
      redirect_uri: window.location.origin,
      audience: process.env.VUE_APP_AUDIENCE,
   })
);

app.mount('#app');

