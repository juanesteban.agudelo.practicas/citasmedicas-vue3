import SignupViewVue from '@/views/SignupView.vue'
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import SignupView from '../views/SignupView.vue'
import ProfileView from '../views/ProfileView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/register',
    name: 'register',
    component: SignupView
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
