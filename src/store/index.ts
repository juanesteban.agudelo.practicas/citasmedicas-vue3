
import { createStore } from 'vuex'

export default createStore({
  state: {
    userLogin: 'TEST',
    usuarios:['pedro', 'juan'],
    usuario: ''
  },
  getters: {
    usuario( state ) {
      return state.usuario;
    },
    usuarios ( state ) {
      return state.usuarios;
    }
  },
  mutations: {
    login( state, string) {
      state.usuarios.push(string);
    },

    loadStore() {
      const storeItem = localStorage.getItem('store')
      if( storeItem ) {
          try {
              this.replaceState(JSON.parse(storeItem));
          }
          catch(e) {
              console.log('Could not initialize store', e);
          }
      }
    },
  },

  actions: {
    addLogin( context, usuario ) {
      context.commit('login', usuario);
    }
  },

  modules: {
  }
})
